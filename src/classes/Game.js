import { fill, includes } from 'lodash'

class Game {
  constructor (players, total) {
    this.players = players
    this.total = total || 301
    this.q = players.length
    this.round = 0
    this.winners = []

    // public
    this.player = 0
    this.over = false
    this.log = []
    this.rests = fill(Array(this.q), this.total)
    this.error = false

    this.start()
  }

  // public

  hit (points) {
    this.error = false

    if (this.over) {
      console.log('game over')
      return
    }

    const validPoints = this.validate(points)

    if (validPoints === false) {
      this.error = `"${points}" - не могу записать такое значение`
      console.log('передано неверное значение')
      return
    }

    this.writeLog(validPoints)
    this.calculateRests()
    this.checkWinners()

    if (!this.over) {
      this.nextTurn()
      this.setActivePlayer()
    }
  }

  get winner () {
    return this.players[this.winners[0]] || false
  }

  // private

  start () {
    this.appendLog()
    this.setActivePlayer()
  }

  validate (points) {
    if (/^\d+$/.test(points) && points <= 180 && points <= this.rests[this.player]) {
      return parseInt(points)
    }
    return false
  }

  writeLog (points) {
    this.score(points)
  }

  calculateRests () {
    this.rests[this.player] -= this.score()
  }

  checkWinners () {
    if (this.rests[this.player] === 0) {
      this.winners.push(this.player)
    }

    if (this.winners.length === this.q - 1) {
      this.over = true
    }
  }

  nextTurn () {
    const player = this.player
    this.nextPlayer()

    if (this.player <= player) {
      this.nextRound()
    }
  }

  nextPlayer () {
    const next = this.player + 1
    this.player = (next === this.q) ? 0 : next

    if (includes(this.winners, this.player)) {
      this.nextPlayer()
    }
  }

  nextRound () {
    this.round++
    this.appendLog()
  }

  appendLog () {
    this.log.push(fill(Array(this.q), ''))
  }

  setActivePlayer () {
    this.log[this.round][this.player] = '*'
  }

  score (points) {
    if (typeof points !== 'undefined') {
      this.log[this.round][this.player] = points
    } else {
      return this.log[this.round][this.player]
    }
  }
}

export default Game
