// For authoring Nightwatch tests, see
// http://nightwatchjs.org/guide#usage

module.exports = {
  'normal game': function (browser) {
    // automatically uses dev Server port from /config.index.js
    // default: http://localhost:8080
    // see nightwatch.conf.js
    const devServer = browser.globals.devServerURL

    browser
      .url(devServer)
      .setValue('input[type=text]', ['Петя', browser.Keys.ENTER])
      .setValue('input[type=text]', ['Люба', browser.Keys.ENTER])
      .click('#startGame')
      .assert.containsText('label', 'Петя')
      .assert.containsText('table th:nth-child(1)', 'Петя')
      .assert.containsText('table th:nth-child(2)', 'Люба')
      .setValue('input[type=text]', ['12', browser.Keys.ENTER])
      .assert.containsText('table th:nth-child(1)', 289)
      .assert.containsText('table th:nth-child(2)', 301)
      .assert.containsText('table td:nth-child(1)', 12)
      .assert.containsText('table td:nth-child(2)', '.')
      .setValue('input[type=text]', ['13', browser.Keys.ENTER])
      .assert.containsText('table th:nth-child(1)', 289)
      .assert.containsText('table th:nth-child(2)', 288)
      .assert.containsText('table td:nth-child(1)', 12)
      .assert.containsText('table td:nth-child(2)', 13)
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .assert.containsText('table th:nth-child(1)', 229)
      .assert.containsText('table th:nth-child(2)', 288)
      .assert.containsText('table td:nth-child(1)', 60)
      .assert.containsText('table td:nth-child(2)', '.')
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .setValue('input[type=text]', ['60', browser.Keys.ENTER])
      .setValue('input[type=text]', ['49', browser.Keys.ENTER])
      .assert.containsText('table th:nth-child(1)', 0)
      // .saveScreenshot('/Users/etalord/www/temp/screen.png')
      // .waitForElementVisible('#app', 5000)
      // .assert.elementPresent('.hello')
      // .assert.containsText('h1', 'Welcome to Your Vue.js App')
      // .assert.elementCount('img', 1)
      .end()
  }
}
