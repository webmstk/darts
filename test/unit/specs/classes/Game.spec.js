/* eslint-disable */
import Game from 'src/classes/Game'

describe('Game', () => {
  beforeEach(() => {
    this.game = new Game(['David', 'Ann', 'Tom'], 101)
  })

  it('correct logs', () => {
    const game = this.game
    const log = game.log
    const rests = game.rests
    const winners = game.winners

    expect(log).to.deep.equal([['*', '', '']])

    // round 1 player 1
    game.hit(71)
    expect(log).to.deep.equal([[71, '*', '']])
    expect(rests).to.deep.equal([30, 101, 101])
    expect(winners).to.deep.equal([])

    // round 1 player 2
    game.hit(51)
    expect(log).to.deep.equal([[71, 51, '*']])
    expect(rests).to.deep.equal([30, 50, 101])
    expect(winners).to.deep.equal([])

    // round 1 player 3
    game.hit(31)
    expect(log).to.deep.equal([[71, 51, 31], ['*', '', '']])
    expect(rests).to.deep.equal([30, 50, 70])
    expect(winners).to.deep.equal([])

    // round 2 player 1
    game.hit(30)
    expect(log).to.deep.equal([[71, 51, 31], [30, '*', '']])
    expect(rests).to.deep.equal([0, 50, 70])
    expect(winners).to.deep.equal([0])

    // round 2 player 2
    game.hit(20)
    expect(log).to.deep.equal([[71, 51, 31], [30, 20, '*']])
    expect(rests).to.deep.equal([0, 30, 70])
    expect(winners).to.deep.equal([0])

    // round 2 player 3
    game.hit(40)
    expect(log).to.deep.equal([[71, 51, 31], [30, 20, 40], ['', '*', '']])
    expect(rests).to.deep.equal([0, 30, 30])
    expect(winners).to.deep.equal([0])

    // round 3 player 2
    game.hit(20)
    expect(log).to.deep.equal([[71, 51, 31], [30, 20, 40], ['', 20, '*']])
    expect(rests).to.deep.equal([0, 10, 30])
    expect(winners).to.deep.equal([0])

    // round 3 player 3
    game.hit(30)
    expect(log).to.deep.equal([[71, 51, 31], [30, 20, 40], ['', 20, 30]])
    expect(rests).to.deep.equal([0, 10, 0])
    expect(winners).to.deep.equal([0, 2])
    expect(game.over).to.equal(true)
  })

  it('anticheat', () => {
    this.game.hit(-20)
    expect(this.game.log).to.deep.eq([['*', '', '']])

    this.game.hit(181)
    expect(this.game.log).to.deep.eq([['*', '', '']])

    this.game.hit('asd')
    expect(this.game.log).to.deep.eq([['*', '', '']])

    this.game.hit(160)
    expect(this.game.log).to.deep.eq([['*', '', '']])

    this.game.hit(0)
    expect(this.game.log).to.deep.eq([[0, '*', '']])
  })

  /*
  it('sets rests', () => {
    const game = this.game

    game.hit(70)
  })

  it('sets winners', () => {
    const game = this.game

    game.hit(70)
    expect(game.rests).to.deep.equal([])

    game.hit(101)
    expect(game.rests).to.deep.equal([1])
  })
  */
})
